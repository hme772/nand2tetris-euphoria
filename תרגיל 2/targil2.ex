--Hodaya Elimelech 318627247
--Yael Malka 316151406
--Targil 2


--goes to C:\Users\yael\euphoria

include std/filesys.e -- for walk_dir
include std/wildcard.e -- for is_match
include std/console.e	-- for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
include std/convert.e -- for to_integer
include std/io.e -- for read_file etc.
with trace -- for trace

--Main Program
integer is_vm --to check if the file is a vm file
integer is_asm --to check if the file is a asm file
integer is_sys_vm --to check if the file is a Sys.vm file
constant TRUE = 1,FALSE = 0
constant IN = 0,OUT = 1
constant EOF = -1
sequence path
sequence tempFile 
object exit_code
atom counter = 0
sequence parts_of_line
integer asmFile
integer vmFile
object line
	
path = prompt_string ("Enter full path to be used:\n")
exit_code = walk_dir(path, routine_id("translate"), TRUE) --we'll go over the directory and translate all VM files to ASM files
exit_code = walk_dir(path, routine_id("codeFinished"), TRUE) --then we'll go over each asm file in the directory and merge them all into one ASM file
if exit_code = -1 then
	printf(OUT, "The folder you chose doesn't exist\n")
else
	printf(OUT, "Executed Successfully\n")
end if
--------------------------------------
-- goes through the files in the chosen folder
function translate(sequence path_name, sequence item) 

	is_vm = is_match("*.vm", item[D_NAME])	
	
	if is_vm then
		--creates an asm file as a translation to the vm file, accesses to the first matching file and works on it
		tempFile = split(item[D_NAME],'.')
		asmFile = open( path_name & "\\" & tempFile[1] & ".asm", "w")--for writing
		sequence fullpath = path_name&"\\"&item[D_NAME]		
		vmFile = open(fullpath, "r")--for reading 
		
		if vmFile = -1 then
			printf(1, "Cannot open file at %s\n", {fullpath})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		if (asmFile) = -1  then
				printf(1, "Can't open file %s\n", {path_name & "\\" & tempFile[1] & ".asm"})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
	while sequence(line) entry do			-- end while when line is equal to -1
		parts_of_line = split_any(line, " \n")	
		
		switch parts_of_line[1] do --the function that needs to operate
			case "pop" then
				pop_function()
			case "push" then
				push_function()
			case "not" then
				not_function()				
			case "or" then
				or_function()
			case "and" then
				and_function()
			case "eq" then
				equal_function()
			case "gt" then
				greater_than_function()
			case "lt" then
				less_than_function()
			case "add" then
				add_function()
			case "sub" then
				sub_function()
			case "neg" then
				negative_function()
			case "label" then
				label_function()
			case "goto" then
				goto_function()
			case "if-goto" then
				if_goto_function()
			case "call" then
				call_function()
			case "function" then
--				funcName = splitLine[2]
				fk_function()
			case "return" then
--				funcName = ""
				return_function()
		end switch
		entry
		line = gets(vmFile) -- reads the next value to operate on
	end while		
		close(vmFile)
		close(asmFile)
	end if
	return 0 --function must return a value it's like a procedure when returning 0		
end function
---------------------------------------------------
function merging_files_function(sequence path_name, sequence item)
--if the file that we are checking right now is an ASM file ,prints whitespace ,opens and reads it into a sequence and writes it into a full asm file
	is_asm = is_match("*.asm", item[D_NAME])	
	integer mergeFile
	if is_asm then
		printf(asmFile, "\n\n\n")
		mergeFile = open(path_name&"\\"&item[D_NAME], "r")
		sequence file_to_join = read_file(mergeFile, io:TEXT_MODE)	
		write_file(asmFile, file_to_join, io:TEXT_MODE)
		close(mergeFile)
	end if
	return 0		
end function


function codeFinished(sequence path_name, sequence item)
	is_sys_vm = is_match("Sys.vm", item[D_NAME])	
	if is_sys_vm then
		-- creates new ASM file with the name of the folder that will be the final one		
		sequence split_path_name = split(path_name, "\\")
		sequence folderName = split_path_name[length(split_path_name)] --gets the name of the folder	
		sequence filePath=path_name & "\\" & folderName & ".asm"
		asmFile = open( filePath , "a") 
		sequence initialization= "@256\nD=A\n@SP\nM=D\n"
		printf(asmFile, "%s", {initialization})--initialization of the file
		parts_of_line = {"call", "Sys.init", "0"}
		call_function()
		walk_dir(path_name, routine_id("merging_files_function"))--merging the files
		
		close(asmFile)
		
	end if
	return 0
end function

---------------------------------------------------
--pop
procedure popToD()
		printf(asmFile, "%s%s%s%s", {"@SP\n","M=M-1\n","A=M\n","D=M\n"})
end procedure

procedure popToA(integer asmFile)
		printf(asmFile, "%s%s%s%s", {"@SP\n","M=M-1\n","A=M\n","A=M\n"})
end procedure
-- D=M+D
function pop_M_and_D_to_D(integer asmFile, object offset, object segment)
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nD=M+D\n@13\nM=D\n"
	printf(asmFile, "%s", {temp})
	popToD()
	printf(asmFile, "%s", {"@13\nA=M\nM=D\n"})
	return 0
end function

-- D=A+D
function pop_A_and_D_to_D(integer asmFile, object offset, object segment)
	sequence temp ="@" & offset & "\nD=A\n@" & segment & "\nD=A+D" & "\n@13" & "\nM=D\n"
	printf(asmFile, "%s", {temp})
	popToD()
	printf(asmFile, "%s", {"@13\nA=M\nM=D\n"})
	return 0
end function
----
procedure pop_function()
	switch parts_of_line[2] do -- parts_of_line[2] is the segment in the memory parts_of_line[3] is the value
		case "local" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "LCL")
			break
		case "argument" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "ARG")
			break
		case "this" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "THIS ")
			break
		case "that" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "THAT ")
			break
		case "temp" then
			pop_A_and_D_to_D(asmFile, parts_of_line[3], "5")		
			break
		case "pointer" then
			pop_A_and_D_to_D(asmFile, parts_of_line[3], "THIS")
			break
		case "static" then			
			popToD()
			sequence final="@" & tempFile[1] & "." & parts_of_line[3] & "\nM=D\n"
			printf(asmFile, "%s", {final})
			break
		case else
			printf(asmFile, "%s", {"ERROR!!! there is no such segment"})
			puts(1,"ERROR!!! there is no such segment\n PROBLEM in the vm file")
			abort(1)
	end switch			
end procedure
-- A=M+D
procedure push_M_and_D_to_A(integer asmFile, object offset, object segment)	
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nA=M+D\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"
	printf(asmFile, "%s", {temp})
end procedure

-- A=A+D
procedure push_A_and_D_to_A(integer asmFile, object offset, object segment)	
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nA=A+D\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"
	printf(asmFile, "%s", {temp})
end procedure
--push
procedure push_function()
	switch parts_of_line[2] do
		case "local" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "LCL")
			break
		case "argument" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "ARG")
			break
		case "this" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "THIS")
			break
		case "that" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "THAT")
			break
		case "temp" then
			push_A_and_D_to_A(asmFile, parts_of_line[3], "5")		
			break
		case "pointer" then
			push_A_and_D_to_A(asmFile, parts_of_line[3], "THIS")
			break
		case "static" then
			sequence final = "@" &tempFile[1] & "." & parts_of_line[3] & "\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"		
			printf(asmFile, "%s", {final})
			break
		case "constant" then
			printf(asmFile, "%s", {"@"& parts_of_line[3] &"\nD=A\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"})
			break
		case else
			printf(asmFile, "%s", {"ERROR!!! there is no such segment"})
			puts(1,"ERROR!!! there is no such segment\n PROBLEM in the vm file")
			abort(1)
	end switch
end procedure
----add,subtract,negative
procedure add_function()
	popToD()
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=M+D\n"})
end procedure

procedure sub_function()
	popToD()
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=M-D\n"})
end procedure

procedure negative_function()
	popToD()
	printf(asmFile, "%s%s%s%s%s", {"@SP\n","A=M\n","M=-D\n","@SP\n","M=M+1\n"})
end procedure
----equal operators
procedure equal_function()
	popToD()
	popToA(asmFile)
	printf(asmFile, "%s%d%s", {"D=D-A\n@IF_TRUE",counter,"\nD; JEQ\n@SP\nA=M\nM=0\n@end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE",counter,")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure
procedure greater_than_function()
	popToD()
	popToA(asmFile)
	printf(asmFile, "%s%d%s%d", {"D=A-D\n@IF_TRUE",counter,"\nD; JGT\n@SP\nA=M\nM=0\n@end",counter+1})	
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE",counter,")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure

procedure less_than_function()
	popToD()
	popToA(asmFile)
	printf(asmFile, "%s%d%s", {"D=A-D\n@IF_TRUE",counter,"\nD; JLT\n@SP\nA=M\nM=0\n@end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE" , counter , ")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure
-----logical operators
procedure and_function()
	popToD()
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=D&M\n"})
end procedure

procedure or_function()
	popToD()
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=D|M\n"})
end procedure

procedure not_function()
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=!M\n"})
end procedure
----------------------------------------------------------
--------------targil2-------------------------------------


--label,goto,if_goto

procedure label_function()
	sequence temp ="(" & tempFile[1] & "." & parts_of_line[2] & ")\n"
	printf(asmFile, "%s", {temp})
end procedure

procedure goto_function()
	sequence temp="@" & tempFile[1] & "." & parts_of_line[2] & "\n0; JMP\n"
	printf(asmFile, "%s", {temp})
end procedure

procedure if_goto_function()
	popToD()
	sequence temp="@" & tempFile[1] & "." & parts_of_line[2] & "\nD; JNE\n"
	printf(asmFile, "%s", {temp})
end procedure

-- push D 

procedure pushD()
	sequence temp ="@SP\nA=M\nM=D\n@SP\nM=M+1\n"
	printf(asmFile, "%s", {temp})
end procedure

-- call f n 
procedure call_function()
	-- @return_address
	printf(asmFile, "%s%d%s", {"@" & tempFile[1] & ".",counter,"\nD=A\n"})
	pushD()
	
	-- push LCL
	printf(asmFile, "%s", {"@LCL\nD=M\n"})
	pushD()
	
	-- push ARG
	printf(asmFile, "%s", {"@ARG\nD=M\n"})
	pushD()
	
	-- push THIS
	printf(asmFile, "%s", {"@THIS\nD=M\n"})
	pushD()
	
	-- push THAT
	printf(asmFile, "%s", {"@THAT\nD=M\n"})
	pushD()
	
	-- ARG = SP-n-5
	printf(asmFile, "%s%s%s", {"@SP\nD=M\n@",parts_of_line[3] & "\n","D=D-A\n@5\nD=D-A\n@ARG\nM=D\n"})

	--	LCL = SP
	printf(asmFile, "%s", {"@SP\nD=M\n@LCL\nM=D\n"})
	
	--	goto f
	printf(asmFile, "%s", {"@" & parts_of_line[2] & "\n"})
	printf(asmFile, "%s", {"0; JMP\n("})
	printf(asmFile, "%s%d%s", {tempFile[1] & ".",counter,")\n"})

	counter = counter + 1

end procedure

 --function declaration 

procedure fk_function()
	integer k = to_integer(parts_of_line[3])
	printf(asmFile, "%s", {"(" & parts_of_line[2] & ")\n"})
	-- repeat k times: push 0
	for i = 1 to k do --pushes 0 k times
		printf(asmFile, "%s", {"@0\nD=A\n"})
		pushD()
	end for
end procedure
 
 ----------------return 
 
 function return_function()
	-- FRAME = LCL ,RET = *(FRAME-5)
	printf(asmFile, "%s%s", {"@LCL\nD=M\n@13\nM=D\n","@5\nD=D-A\nA=D\nD=M\n@14\nM=D\n"})

	popToD()
	
	-- *ARG = POP()	,SP = ARG+1 ,THAT = *(FRAME-1) ,THIS = *(FRAME-2) ,ARG = *(FRAME-3)
	
	printf(asmFile, "%s%s%s%s%s", {"@ARG\nA=M\nM=D\n","D=A\n@SP\nM=D+1\n","@13\nA=M-1\nD=M\n@THAT\nM=D\n","@13\nA=M-1\nA=A-1\nD=M\n@THIS\nM=D\n","@13\nA=M-1\nA=A-1\nA=A-1\nD=M\n@ARG\nM=D\n"})
	
	-- LCL = *(FRAME-4) ,goto RET
	printf(asmFile, "%s%s", {"@13\nA=M-1\nA=A-1\nA=A-1\nA=A-1\nD=M\n@LCL\nM=D\n","@14\nA=M\n0;JMP\n"})

	return 0	
end function
 
 
 



