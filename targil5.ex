--Hodaya Elimelech 318627247
--Yael Malka 316151406
--Targil 5


--goes to C:\Users\yael\euphoria

include std/pretty.e
include std/filesys.e -- for walk_dir
include std/wildcard.e -- for is_match
include std/console.e	-- for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
include std/convert.e -- for to_integer
include std/io.e -- for read_file etc.
with trace -- for trace
include std/map.e
include std/math.e 



------ walk dir vars
sequence  xmlpath, Txmlpath, fName
sequence  data--that we read during the conversion 
integer current --the current value we are focusing on
integer last

--Main Program
integer is_jack --to check if the file is a jack file
integer is_asm --to check if the file is a asm file
integer is_sys_vm --to check if the file is a Sys.vm file
constant TRUE = 1,FALSE = 0
constant IN = 0,OUT = 1
constant EOF = -1
sequence path
sequence tempFile 
object exit_code
atom counter = 0
sequence parts_of_line
integer jackFile,vmFile,TxmlFile,xmlFile
object line

--jack constants
sequence keyword = {"class","constructor","function","method","field","static","var","int","char","boolean","void","true","false","null","this","let","do","if","else","while","return"}
sequence symbol = "{}()[].,;+-*/&|<>=~}"
sequence symbolAscii = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~',' ','\t','"','\n'}
sequence keywordConstant={"true","false","null","this"}

---------------------------------------------
-------------Targil 5------------------------
---------------------------------------------


-------------------------------------
map class_scope_table = map:new() 
map methods_scope_table = map:new()
constant Table_NAME = 1, Table_TYPE = 2, Table_KIND = 3, Table_NUM = 4
sequence className, funcName
integer argCounter = 0, varCounter = 0, fieldCounter = 0, staticCounter = 0, ifCounter = 0, subroutineDecFlag = 0


-------------------------------------------------------
path = prompt_string ("Enter full path to be used:\n")
exit_code = walk_dir(path, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "The folder you chose doesn't exist\n")
else
	printf(OUT, "Executed Successfully\n")
end if



-- goes through the files in the chosen folder
function look_at(sequence path_name, sequence item) 

	is_jack = is_match("*.jack", item[D_NAME])	
	
	if is_jack then
		--creates vm file as translation to the jack file, accesses to the first matching file and works on it
		tempFile = split(item[D_NAME],'.')
		--for writing into a vm file
		vmFile= open( path_name & "\\" & tempFile[1] & ".vm" ,"w")
		--TxmlFile = open( path_name & "\\" & tempFile[1] & "TOur.xml", "w")
		sequence fullpath = path_name&"\\"&item[D_NAME]		
		jackFile = open(fullpath, "r")--for reading 
		
		
		if jackFile = -1 then
			printf(1, "Cannot open file at %s\n", {fullpath})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		if (vmFile) = -1  then
				printf(1, "Can't open file %s\n", {path_name & "\\" & tempFile[1] & ".vm"})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		

			
		following_char_function()
		sequence data = class_function()
		--TokenizingFile_function(data)	
		parsing_function(data)
		close(vmFile)
		--close(TxmlFile)
		close(jackFile)
	end if
	return 0 --function must return a value it's like a procedure when returning 0		
end function


--gets us the next char to read  
procedure following_char_function()
	current = getc(jackFile)
	skipWhiteSpace_function()
end procedure



-- skips white spaces and comments too (9=tab)
procedure skip_comments_spaces_function() 
	while (current = '/' or current = ' ' or current = '\n' or current = 9) do
		if (current = ' ' or current = '\n' or current = 9) then
				skipWhiteSpace_function()
		end if
		if current = '/' then
			following_char_function()
			switch current do
				case '*' then--/*
					skip_comments_function()
				case '/' then --//
					gets(jackFile)--skips a line of a comment (reads ,ignores and goes to the next line
					following_char_function()
				
				case else
					printf(OUT, "error in skip_comments_spaces_function")
			end switch
		end if
	end while
end procedure


--checks if /* appeared  on the file and */ appears too, ignores anything between them
procedure skip_comments_function()  
  current = getc(jackFile) 
  last=0
  while (not (last = '*' and current = '/')) do
	last = current
	current = getc(jackFile)
  end while
  -- after finding */ read the next char
  following_char_function()
end procedure

----skipWhiteSpace_function
procedure skipWhiteSpace_function() --9=tab
	while (current = ' ' or current = '\n' or current = 9) do
		current = getc(jackFile)
	end while
end procedure


----------------------------------------------------------------
----tokenizing
function TokenizingFile_function(sequence res, integer level=0) 
	integer lev
	printf(xmlFile , "<" & res[1] &">")
	
	if (sequence(res[2])and equal(res[2],{})) then
		printf(xmlFile , "\n")
		lev=level-1 --for the first time there will be no space
		for i=0 to lev do
			printf(xmlFile, "  ")--prints double space
		end for
		printf(xmlFile , "</" & res[1] & ">")
	elsif (atom(res[2][1])) then
		printf(xmlFile ,( " " & res[2] & " </" & res[1] & ">"))
	else --if res holds more than 1 value 
		for i=1 to length(res[2]) do
			printf(xmlFile , "\n")
			for j=0 to level do
				printf(xmlFile, "  ")--prints double space
			end for
			TokenizingFile_function(res[2][i], level+1 )
		end for
		printf(xmlFile , "\n")
		lev=level-1 --for the first time there will be no space
		for i=0 to lev do
			printf(xmlFile, "  ")--prints double space
		end for
		printf(xmlFile , "</" & res[1] & ">")
	end if	
	return 0
end function


--------------------------------------------------------------------
------pop
procedure pop_function(sequence kind_of_var, object number_of_var)
	printf(vmFile, "pop " & kind_of_var & " ")
	print(vmFile, number_of_var)
	printf(vmFile, "\n")
end procedure

-----push
function push_function(sequence kind_of_var, integer number_of_var)
	printf(vmFile, "push %s %d\n",{kind_of_var,number_of_var})
	return 0
end function



--------------------------------------------------------------------
----parsing

procedure parsing_function(sequence res)  
--previous version (targil4)
	--printf(TxmlFile , "<tokens>\n")
	--Txml_printing_function(res)
	--printf(TxmlFile , "</tokens>")
	if sequence(res) and equal(res ,{}) then		
		-- do nothing		
	elsif string_or_not(res[1]) then -- for tag and sequence values
		switch res[1] do
			case "var_declaration" then
				var_declaration_tag_function(res[2])
			case "parameter_list" then
				parameter_list_tag_function(res[2])
			case "subroutine_declaration" then  --incase of a function it needs to look at the second token
				subroutine_declaration_tag_function(res[2])
				parsing_function(res[2])
			case "class" then --incase of a class it needs to look at the second token
				class_tag_function(res[2])
				parsing_function(res[2])
			case "class_var_declaration" then
				class_var_declaration_tag_function(res[2])
			case "if_statement" then --
				print_subroutine_declaration_function()
				if_statement_tag_function(res[2])
			case "while_statement" then --
				print_subroutine_declaration_function()
				while_statement_tag_function(res[2])
			case "do_statement" then
				print_subroutine_declaration_function()
				do_statement_tag_function(res[2])
			case "let_statement" then
				print_subroutine_declaration_function()
				let_statement_tag_function(res[2])
			case "return_statement" then
				print_subroutine_declaration_function()
				return_statement_tag_function(res[2])
			case else
				if sequence(res[2]) and not(string_or_not(res[2])) then	
					for i=1 to length(res[2]) do			
						parsing_function(res[2][i])
					end for
				end if
		end switch
	else -- for sequence value
		for i=1 to length(res) do			
			parsing_function(res[i])
		end for				
	end if
	
end procedure

procedure Txml_printing_function(sequence res)  
	if (sequence(res[2])and equal(res[2],{})) then
	elsif (atom(res[2][1])) then
		printf(TxmlFile ,( "<" & res[1] &"> " & res[2] & " </" & res[1] & ">\n"))
	else
		for i=1 to length(res[2]) do
			Txml_printing_function(res[2][i])
		end for
	end if
	
end procedure

--returns a word
function word_function(sequence cond = symbolAscii)
  integer condition=not find(current, cond)
  sequence word = ""  
  while (condition) do
	word = word & current
	current = getc(jackFile)
	condition=not find(current, cond)
  end while
  
  skipWhiteSpace_function()
  return word
  
end function


-----------------------------------------------------
---- lexical elements

-- integer_constant
function integer_constant_function()
  integer condition=find(current, {48,49,50,51,52,53,54,55,56,57})
  sequence word = ""  
  while (condition) do
	word = word & current
	current = getc(jackFile)
	condition=find(current, {48,49,50,51,52,53,54,55,56,57})
  end while
  skipWhiteSpace_function()
	return {"integer_constant", word}
end function

-- string_constant
function string_constant_function()
	following_char_function()	-- for '"'
	sequence data = {"string_constant", word_function({'"'})}
	following_char_function()	-- for '"'
	return data	
end function

-- keyword_constant
function keyword_constant_function(sequence w = "")
	if equal(w,"") then
		w = word_function()
	end if
return {"keyword", w}
end function

--identifier
function identifier_function(sequence currentWord="")	
	if (equal(currentWord, "")) then
		currentWord = word_function()
	end if
	return {"identifier", currentWord}
end function


--------------------------------------------------------------------
-----operators

--can have white space before the op
function unary_op_function()
	data = {}
	skipWhiteSpace_function()
		switch current do
			case '~' then
				data = data & "~"
			case '-' then
				data = data & "-"
			case else
				printf(OUT, "there is no such unary operator")
		end switch
		following_char_function()
	return {"symbol" , data}
end function

--can have white space before the op
function op_function()
	data = {}
	switch current do
		case '*' then
			data = data & "*"
		case '/' then 
			data = data & "/"
		case '+' then
			data = data & "+"
		case '-' then
			data = data & "-"
		case '<' then
			data = data & "&lt;"
		case '>' then
			data = data & "&gt;"
		case '=' then
			data = data & "="
		case '&' then
			data = data & "&amp;"	
		case '|' then
			data = data & "|"				
		case else
			printf(OUT, "there is no such operator")
	end switch

	following_char_function()
	return {"symbol" ,data}
end function



---------------------------------------------------------------------------
----statements

--statement 
function statement_function()
	sequence data = {}
	sequence result = {}
	sequence word = word_function()
	switch word do
		case "if" then
			result =  if_Statement_function()
		case "while" then
			result =  while_statement_function()
		case "do" then
			result =  do_statement_function()
		case "let" then
			result =  let_Statement_function()
		case "return" then
			result =  return_statement_function()				
		case else
			printf(OUT, "there is no such statement")
	end switch
	data = data &result
return data
end function

--statements creates a list of statements 1 by 1
function statements_function()
	sequence data = { }
	skip_comments_spaces_function()
	integer condition =not (current = '}') -- there is only one option for a statement in the subroutineBody before }
	while ( condition ) do
		data = append(data, statement_function())
		skip_comments_spaces_function()
		condition =not (current = '}')
	end while

	return {"statements", data}
end function

--if_Statement
function if_Statement_function()
	sequence data = { }
	data = append(append(data,{"keyword", "if"}),{"symbol", "("})
	following_char_function()
	data = append(append(data, expression_function()),{"symbol", ")"})
	following_char_function()
	skip_comments_spaces_function() -- incase of comments in the if
	data = append(data,{"symbol", "{"})
	following_char_function()
	data = append(append(data, statements_function()),{"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function() -- incase of comments after the if
		if (equal(current,'e') and equal(word_function(),"else")) then
			data = append(append(data, {"keyword","else"}), {"symbol","{"})
			following_char_function()
			skip_comments_spaces_function() --incase of comments in the else
			data = append(append(data, statements_function()), {"symbol","}"})
			following_char_function()
			skip_comments_spaces_function() -- incase of comments after the else
		end if	
return {"if_statement", data}
end function

--while_statement_function
function while_statement_function()
	sequence data = {}
	data = append(append(data,{"keyword", "while"}),{"symbol", "("})
	-- word_function in statements_function made skipWhiteSpace_function

	following_char_function()
	data = append(append(data, expression_function()),{"symbol", ")"})
	following_char_function()
	skip_comments_spaces_function() -- if there is a comment inside the while (before{)

	data = append(data,{"symbol", "{"})
	skip_comments_spaces_function()-- if there is a comment inside the while (after{) 
	following_char_function()
	--statements_function() skips
	data = append(append(data, statements_function()),{"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function() --if there is a comment after the while
	return {"while_statement", data}
end function

--do_statement_function
function do_statement_function()
	sequence data = {}
	data = append(data,{"keyword", "do"})-- next char was read by word_function()
	data = append(data & subroutine_call_function(),{"symbol", ";"})
	following_char_function()		

return {"do_statement" ,  data}
end function

---let gets an identifier and if  not = is after it is an array
function let_Statement_function()
	sequence data = { }
	data = append(append(data,{"keyword", "let"}), identifier_function())	
	if (current = '[') then
		data = append(data, {"symbol","["})
		following_char_function()
		data = append(append(data, expression_function()), {"symbol","]"})
		following_char_function()
	end if
		data = append(data,{"symbol", "="})
		following_char_function()
		data = append(append(data, expression_function()),{"symbol", ";"})
		following_char_function()
return {"let_statement", data}
end function

--return_statement_function
function return_statement_function()
	sequence data = {}
	data = append(data,{"keyword", "return"})	
	if (current != ';') then --if that is not the end of the return 
		data = append(data, expression_function())--there must be an expression to add
	end if
	data = append(data,{"symbol", ";"})
	following_char_function()		
return {"return_statement" , data}
end function

-------------------------------------------------------
-- class_function

--this function starts everything. creates the token
function class_function()
	sequence data = {}
	skip_comments_spaces_function()
	data = append(data,{"keyword", "class"})
	word_function()
	data = append(data, identifier_function())
	skip_comments_spaces_function()
	data = append(data, {"symbol", "{"})
	following_char_function()
	skip_comments_spaces_function()
	data = data & classVar_SubroutineDec_function()
	data = append(data, {"symbol", "}"})
	following_char_function()
	return {"class", data}
end function

--class_tag_function 
procedure class_tag_function(sequence res)
	className = res[2][2]--({class,main,etc
	clear(class_scope_table)
	fieldCounter = 0
	staticCounter =0 
	ifCounter = 0
end procedure

-- class_var_declaration_tag_function 
procedure class_var_declaration_tag_function(sequence res)
	if equal(res[1][2], "static") then
		for i=3 to length(res) by 2 do	--jumps by 2 static int id		
			put(class_scope_table, res[i][2], {res[2][2] ,"static" , staticCounter}) -- name; type=int,float,etc; kind=var,static etc, counter
			staticCounter = staticCounter + 1
		end for

	else --field
		for i=3 to length(res) by 2 do	--jumps by 2 field int id		
			put(class_scope_table, res[i][2], {res[2][2] ,"this" , fieldCounter}) -- name; type=int,float,etc; kind=var,this, etc, counter
			fieldCounter = fieldCounter + 1
		end for
	end if
end procedure

-- var_declaration_tag_function
procedure var_declaration_tag_function(sequence res)
	for i=3 to length(res) by 2 do--jumps by 2 because it's written as: var type name; (var int a;)			
		put(methods_scope_table, res[i][2], {res[2][2] ,"local" , varCounter}) -- name; type=int,float,etc; kind=var,arg,local etc, counter
		varCounter = varCounter + 1
	end for
end procedure

-- subroutine_declaration_tag_function
procedure subroutine_declaration_tag_function(sequence res)
	clear(methods_scope_table)--clears the methods table
	argCounter =0
	varCounter =0 
	subroutineDecFlag =0 --puts 0 in the counters
	funcName=res[3][2]
	sequence funcType = res[1][2]
	
	--to distinguish between types of functions
	if equal(funcType, "method") then --if it's a method, it must be called be an instance of the class (using this)
		put(methods_scope_table, "this", {className ,"argument" , argCounter}) -- name; type=name of the class,etc; kind=var,arg,local etc, counter
		argCounter = argCounter + 1
		subroutineDecFlag = 3
	end if
	if equal(funcType, "constructor") then
		subroutineDecFlag = 1
	end if
	if equal(funcType, "function") then
		subroutineDecFlag = 2
	end if
end procedure

-- print_subroutine_declaration_function
procedure print_subroutine_declaration_function()
	integer condition=(subroutineDecFlag != 0)
	if (condition) then
		printf(vmFile, "function " & className & "." & funcName & " %d\n", varCounter)
	else return
	end if
	switch subroutineDecFlag do
		case 1 then
			push_function("constant", fieldCounter)
			printf(vmFile,"call Memory.alloc 1\n")
			pop_function("pointer", to_integer(0))		
		case 2 then
		case 3 then
			push_function("argument", 0)
			pop_function("pointer", to_integer(0))
	end switch
	subroutineDecFlag = 0
end procedure

-- parameter_list_tag_function
procedure parameter_list_tag_function(sequence res)
	integer condition= sequence(res) and equal(res ,{})
	if (condition) then
		-- does nothing when there are no parameters
	else
		for i=1 to length(res) by 3 do --incase of having parameters, jumps by 3 because ( int a , int b =	( and , are tokens
		put(methods_scope_table, res[i+1][2], {res[i][2] ,"argument" , argCounter})  -- name; type=int,float,etc; kind=var,arg,local etc, counter
		argCounter = argCounter + 1
	end for
	end if
end procedure

-- push_var_name_function
procedure push_var_name_function(sequence res)
	object data = get(methods_scope_table, res)	-- this var appears in the class table
	integer condition
	condition=(atom(data) and equal(data,0))
	if condition then	
		data = get(class_scope_table, res)-- this var appears in the methods table
	end if
	push_function(data[2],data[3])
end procedure

-- pop_var_name_function 
procedure pop_var_name_function(sequence res)
	object data = get(methods_scope_table, res)-- this var appears in the methods table
	integer condition
	condition= (atom(data) and equal (data,0))
	if condition then	-- this var appears in the class table
		data = get(class_scope_table, res)
	end if
	pop_function(data[2],data[3])
end procedure

----push var with expression
procedure push_var_plus_expression_function(sequence var, sequence exp)
	expression_tag_function(exp)
	push_var_name_function(var)
	printf(vmFile , "add\npop pointer 1\npush that 0\n")
end procedure

--this function responsible for case there is a declaration on a class type variable 
function classVar_SubroutineDec_function()
	sequence curWord = word_function()
	  integer condition= equal(curWord, "static" ) or equal(curWord, "field" )
	sequence data ={}
	while (condition) do
		data = append(data, class_var_dec_function(curWord))
		skip_comments_spaces_function()
		curWord = word_function()
		condition= equal(curWord, "static" ) or equal(curWord, "field" )
	end while
	
	
	while (current != '}') do --if it's not the end
		data = append(data, subroutine_dec_function(curWord))-- add the rest of class
		skip_comments_spaces_function()
		curWord = word_function()
	end while
	
	return data
end function

--class_var_declaration responsible for variables' decleration
function class_var_dec_function(sequence curWord)
	sequence data = {}
	
	data = append(append(append(data,{"keyword", curWord}), type_function()), identifier_function()) --for the first id
		
	while (current != ';') do --if there is more than 1 id
		data = append(data, {"symbol", ","})
		following_char_function()
		data = append(data, identifier_function())
	end while
	
	data = append(data,{"symbol", ";"}) --the endl
	following_char_function()
	
	return {"class_var_declaration", data}
end function

-- type can be int, boolean, char
function type_function(sequence curWord="")
	integer condition
	condition = equal(curWord, "")
	if (condition) then
		curWord = word_function() --reads the word after the empty token
	end if
	
	condition = equal(curWord, "int") or equal(curWord, "boolean") or equal(curWord, "char")
	
	if (condition) then
		data = {"keyword", curWord}
	else --if it is not a type it is an identifier
		data = {"identifier", curWord}
	end if
	
	return data
end function

-- this function decides what is the type of the function (void/regular type such as int etc.)	
function subroutine_dec_function(sequence curWord)
	sequence data = {}

	data = append(data,{"keyword", curWord})
	curWord = word_function()	
	
	if (not(equal(curWord ,"void"))) then --all types of functions but void
		data = append(data,type_function(curWord))
	else
		data = append(data,{"keyword", "void"})
	end if
	--we add ( then parameters then) and the body 
	data = append(append(data,identifier_function()), {"symbol", "("})
	following_char_function()
	
	data = append(append(data,parameter_list_function()), {"symbol", ")"})
	following_char_function()
	data = append(data,subroutine_body_function())
	
	return {"subroutine_declaration", data}
end function

--parameter_list_function deals with a list of parameters 
function parameter_list_function()
	sequence data = {}
	integer condition = (current != ')')
	if (condition) then --for the first parameter
		data = append(append(data,type_function()),identifier_function())
	
		while equal(current,',') do -- in case of more than 1 parameter
			data = append(data, {"symbol", ","})
			following_char_function()
			data = append(append(data,type_function()),identifier_function())
			
		end while
	end if	
	return {"parameter_list", data}
end function

-- subroutine_body_function has list of variables and statements
function subroutine_body_function()
	sequence data = {}
	skip_comments_spaces_function()
	data = append(data, {"symbol", "{"})
	following_char_function()
	skip_comments_spaces_function()
	
	while (current = 'v') do --the variables are all together
		data = append(data, var_dec_function())
	end while
	
	data = append(data, statements_function())--the statements of the class
	skip_comments_spaces_function()
	data = append(data, {"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function()
	return {"subroutineBody", data}
end function

--var_dec_function responsible to handle variables' decleration
function var_dec_function()
	sequence data = {}
	
	data = append(append(append(data, {"keyword", word_function()}), type_function()), identifier_function())
	
	while (current != ';') do --more than 1 variable
		data = append(data, {"symbol", ","})
		following_char_function()
		data = append(data, identifier_function())
	end while
	
	data = append(data,{"symbol", ";"})	
	following_char_function()
	
	return {"var_declaration", data}
end function

-- Expressions 
-- expression_function 
function expression_function()
	sequence data = {}
	data = append(data,term_function())
	integer condition =not ( current = ')' or current = ',' or current = ';' or current = ']' )
	while (condition) do --list of expressions (1 or more)
		data = append(append(data, op_function()), term_function())
		condition =not ( current = ')' or current = ',' or current = ';' or current = ']' )
	end while
return {"expression", data}
end function

--term_function
function term_function()
	sequence data = {}, word
	--parameters and ops
	if( current = '~' or current = '-' ) then 
		data = append(append(data, unary_op_function()), term_function())
	elsif(current >= 48 and current <= 57)then
		data = append(data, integer_constant_function())
	elsif(current = '"')then
		data = append(data, string_constant_function())
	elsif(current = '(')then
		data = append(data, {"symbol", "("})
		following_char_function()
		data = append(append(data, expression_function()),{"symbol", ")"})
		following_char_function()
	else
		word = word_function()
		if(find (word,keywordConstant)) then
			data = append(data, keyword_constant_function(word))
		elsif (current = '.') or (current = '(') then --function
			data = data & subroutine_call_function(word)
		elsif(current = '[')then --array
			data = append(append(data,identifier_function(word)),{"symbol", "["})
			following_char_function()
			data = append(append(data, expression_function()),{"symbol", "]"})
			following_char_function()
		else
			data = append(data,identifier_function(word))
		end if
	end if
	
return {"term", data}
end function

-- expression_list_function 
function expression_list_function()
	sequence data = {}
	if( not (current = ')')) then
		data = append(data, expression_function())
		while (not (current = ')')) do --more than 1 express
			data = append(data, {"symbol", ","})
			following_char_function()
			data = append(data, expression_function())
		end while
	end if
return {"expressionList", data}
end function

--subroutine_call_function 
function subroutine_call_function(sequence word = "")
	if equal(word,"") then
		word = word_function()
	end if
	sequence data = {identifier_function(word)}
	if(current = '.') then
		data = append(data,{"symbol", "."})
		following_char_function()
		data = append(data, identifier_function())
	end if

		data = append(data,{"symbol", "("})
		following_char_function()
		data = append(append(data, expression_list_function()),{"symbol", ")"})
		following_char_function()
	return data
end function

--tags
-- if_statement_tag_function
procedure if_statement_tag_function(sequence res)
	integer localCounter = ifCounter
	ifCounter = ifCounter + 1
	
	expression_tag_function(res[3][2])-- condition
	
	printf(vmFile, "if-goto IF_TRUE")	-- if the condition is TRUE, jump to inside
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")
	printf(vmFile, "goto IF_FALSE")		-- if the condition isn't TRUE, jump to FALSE
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")
	printf(vmFile, "label IF_TRUE")		-- if TRUE, jump to here
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")
	
	parsing_function(res[6][2])				-- if TRUE do
	
	printf(vmFile, "goto end")		-- if the condition is TRUE, jump to over "else"
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")
	
	printf(vmFile, "label IF_FALSE")	-- if FALSE
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")	
	
	if length(res) > 7 then
		parsing_function(res[10])		-- else
	end if
	
	printf(vmFile, "label end")		-- end if
	printf(vmFile, "%d" , {localCounter})
	printf(vmFile, "\n")
	
end procedure

-- while_statement_tag_function
procedure while_statement_tag_function(sequence res)
	integer localCounter = ifCounter
	ifCounter = ifCounter + 1
 -- the while
	printf(vmFile, "%s%d\n" , {"label WHILE_CONDITION",localCounter})
	expression_tag_function(res[3][2])    		-- condition
	
	printf(vmFile, "not\nif-goto IF_FALSE")
  -- if the condition is false, goto the end of the while
	printf(vmFile, "%d\n" , {localCounter})
	parsing_function(res[6][2])    -- if TRUE do statements

  -- goto the beginning
	printf(vmFile, "goto WHILE_CONDITION%d\nlabel IF_FALSE" , {localCounter})
  -- end of the while when the  condition is false
	printf(vmFile, "%d\n" , {localCounter})
end procedure

-- do_statement_tag_function
procedure do_statement_tag_function(sequence res)
	sequence subCall = {}
	for i=2 to length(res) do --appends the list of tokens
		subCall = append(subCall, res[i])
	end for
	
	subroutine_call_procedure(subCall)--subroutine_call, calls the function
	pop_function("temp", 0)
end procedure

-- let_statement_tag_function
procedure let_statement_tag_function(sequence res)
	expression_tag_function(res[4][2])
	if equal(res[3][2],"=") then -- let var x= kuku;
		pop_var_name_function(res[2][2])
	else	-- let var[exp] = something;
		push_var_name_function(res[2][2])
		printf(vmFile , "add\n")
		expression_tag_function(res[7][2])
		--handles the stack
		pop_function("temp", 0)
		pop_function("pointer", 1)
		push_function("temp", 0)
		pop_function("that", 0)
	end if
end procedure

-- return_statement_tag_function
procedure return_statement_tag_function(sequence res)
	sequence temp = res[2][2]
	if sequence(res[2][2]) and equal(res[2][2],";") then
		push_function("constant", 0)
	else
		expression_tag_function(res[2][2])
	end if
	printf(vmFile , "return\n")
end procedure

-- term_tag_function
procedure term_tag_function(sequence res)
	sequence tag = res[1][1]
	sequence val = res[1][2]
	switch tag do
		case "keyword" then
			switch val do
				case "true" then
					push_function("constant", 0)
					printf(vmFile , "not \n")	
				case "false" then
					push_function("constant", 0)
				case "this" then
					push_function("pointer", 0)					
				case "null" then
					push_function("constant", 0)
			end switch
		case "symbol" then
			switch val do
				case "~" then
					term_tag_function(res[2][2])
					unary_op_tag_function(val)
				case "(" then
					expression_tag_function(res[2][2])	
				case "-" then
					term_tag_function(res[2][2])
					unary_op_tag_function(val)
			end switch
		case "identifier" then
			if (length(res) =  1) then	-- varName
				push_var_name_function(val)
			else
				object valojb = res[2][2]
				switch valojb do
					case "(" then	-- subroutineName(expressionList)
						subroutine_call_procedure(res)
					case "[" then	-- varName [expression]
						push_var_plus_expression_function(res[1][2], res[3][2])
					case "." then	-- className|varName.subroutineName(expressionList)
						subroutine_call_procedure(res)
					case else	-- varName
						push_var_name_function(res[1][2])
				end switch
			end if
		case "string_constant" then
			string_constant_tag_function(res[1][2])
		case "integer_constant" then
			push_function("constant", to_integer((res[1][2])))
	end switch
end procedure

-- string_constant_tag_function
procedure string_constant_tag_function(sequence res)
	integer len = length(res)
	push_function("constant", len)
	printf(vmFile , "call String.new 1\n")
	
	for i=1 to len do
		push_function("constant", to_integer(res[i]))
		printf(vmFile , "call String.appendChar 2\n")
	end for
end procedure

-- subroutine_call_procedure					
procedure subroutine_call_procedure(sequence res)
	sequence var = res[2][2]
	switch var do 
		case "." then	-- className|varName.subroutineName(expressionList)
			integer numArg = ceil(length(res[5][2])/2)
			
			object data = get(methods_scope_table, res[1][2])
			if atom(data) and data = 0 then	
				data = get(class_scope_table, res[1][2])
			end if
			
			if sequence(data) then	-- if its a variable name
				push_function(data[2],data[3])
				numArg = numArg + 1
				expression_list_tag_function(res[5][2])	--pushes the list of parameters
				printf(vmFile , "call " & data[1] & res[2][2] & res[3][2] & " %d\n",{numArg})	-- print call variable.funcName numArg
			else	-- if its a class name	
				expression_list_tag_function(res[5][2])	--pushes the list of parameters
				printf(vmFile , "call " & res[1][2] & res[2][2] & res[3][2] & " %d\n",{numArg})	-- print call class.funcName numArg
			end if
		case "(" then	-- subroutineName(expressionList)
			integer numArg = ceil(length(res[3][2])/2)
			push_function("pointer", 0)
			numArg = numArg + 1
			expression_list_tag_function(res[3][2])	--pushes the list of parameters
			printf(vmFile , "call " & className & "." & res[1][2] & " %d\n",{numArg})	-- print call class.funcName numArg
	end switch
end procedure

-- unary_op_tag_function
procedure unary_op_tag_function(object res)
	switch res[1] do
		case '~' then
			printf(vmFile , "not \n")
		case '-' then
			printf(vmFile , "neg \n")
		case else
			printf(OUT, "something went wrong on unary_op_tag_function()")
	end switch
end procedure

-- op_tag_function chooses the operation
procedure op_tag_function(object res)
	sequence var = res
	switch var do
		case "/" then 
			printf(vmFile , "call Math.divide 2\n")
		case "*" then
			printf(vmFile , "call Math.multiply 2\n")
		case "-" then
			printf(vmFile , "sub\n")
		case "+" then
			printf(vmFile , "add\n")
		case "=" then
			printf(vmFile, "eq\n")
		case "|" then
			printf(vmFile, "or\n")
		case "&lt;" then
			printf(vmFile, "lt\n")
		case "&gt;" then
			printf(vmFile, "gt\n")
		case "&amp;" then
			printf(vmFile, "and\n")
		case else
			printf(OUT, "something went wrong on op_tag_function()")
	end switch
end procedure

-- expression_tag_function
procedure expression_tag_function(sequence res)
	term_tag_function(res[1][2])--the operation to do 
	
	for i=2 to length(res) by 2 do	
		sequence firstVal = res[i][2]
		sequence secondVal = res[i+1][2]
		term_tag_function(res[i+1][2])
		op_tag_function(res[i][2])
	end for
end procedure

-- expression_list_tag_function 
procedure expression_list_tag_function(sequence res)
	if sequence(res) and equal(res ,{}) then
		--when there is no expression does nothing	
	else
		expression_tag_function(res[1][2])--ignores the word let
		for i=2 to length(res) by 2 do	--looks for the operation to do		
			expression_tag_function(res[i+1][2])
		end for
	end if
end procedure  

function string_or_not(sequence s)  
	integer str = 0
	if sequence(s) then
	 if equal(s ,{}) then
		str = 0
	 elsif atom(s[1]) then
		str = 1
	 end if
	end if
	return str
end function