--Hodaya Elimelech 318627247
--Yael Malka 316151406
--Targil 4


--goes to C:\Users\yael\euphoria

include std/filesys.e -- for walk_dir
include std/wildcard.e -- for is_match
include std/console.e	-- for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
include std/convert.e -- for to_integer
include std/io.e -- for read_file etc.
with trace -- for trace


------ walk dir vars
sequence  xmlpath, Txmlpath, fName
sequence  data--that we read during the conversion 
integer current --the current value we are focusing on
integer last

--Main Program
integer is_jack --to check if the file is a jack file
integer is_asm --to check if the file is a asm file
integer is_sys_vm --to check if the file is a Sys.vm file
constant TRUE = 1,FALSE = 0
constant IN = 0,OUT = 1
constant EOF = -1
sequence path
sequence tempFile 
object exit_code
atom counter = 0
sequence parts_of_line
integer jackFile, xmlFile, TxmlFile
object line

--jack constants
sequence keyword = {"class","constructor","function","method","field","static","var","int","char","boolean","void","true","false","null","this","let","do","if","else","while","return"}
sequence symbol = "{}()[].,;+-*/&|<>=~}"
sequence symbolAscii = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~',' ','\t','"','\n'}
sequence keywordConstant={"true","false","null","this"}


path = prompt_string ("Enter full path to be used:\n")
exit_code = walk_dir(path, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "The folder you chose doesn't exist\n")
else
	printf(OUT, "Executed Successfully\n")
end if
--------------------------------------
-- goes through the files in the chosen folder
function look_at(sequence path_name, sequence item) 

	is_jack = is_match("*.jack", item[D_NAME])	
	
	if is_jack then
		--creates xml files as translation to the jack file, accesses to the first matching file and works on it
		tempFile = split(item[D_NAME],'.')
		--for writing into 2 xml files
		xmlFile = open( path_name & "\\" & tempFile[1] & "Our.xml", "w")
		TxmlFile = open( path_name & "\\" & tempFile[1] & "TOur.xml", "w")
		sequence fullpath = path_name&"\\"&item[D_NAME]		
		jackFile = open(fullpath, "r")--for reading 
		
		if jackFile = -1 then
			printf(1, "Cannot open file at %s\n", {fullpath})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		if (xmlFile) = -1  then
				printf(1, "Can't open file %s\n", {path_name & "\\" & tempFile[1] & "Our.xml"})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		if (TxmlFile) = -1  then
				printf(1, "Can't open file %s\n", {path_name & "\\" & tempFile[1] & "TOur.xml"})
			any_key ("\n   Press any key to continue... ")--escape
		end if 

			
		following_char_function()
		sequence data = class_function()
		TokenizingFile_function(data)
		parsing_function(data)	
	
		close(xmlFile)
		close(TxmlFile)
		close(jackFile)
	end if
	return 0 --function must return a value it's like a procedure when returning 0		
end function
---------------------------------------------------
--gets us the next char to read  
procedure following_char_function()
	current = getc(jackFile)
	skipWhiteSpace_function()
end procedure
----------------------------------------------------
-- skips white spaces and comments too (9=tab)
procedure skip_comments_spaces_function() 
	while (current = '/' or current = ' ' or current = '\n' or current = 9) do
		if (current = ' ' or current = '\n' or current = 9) then
				skipWhiteSpace_function()
		end if
		if current = '/' then
			following_char_function()
			switch current do
				case '*' then--/*
					skip_comments_function()
				case '/' then --//
					gets(jackFile)--skips a line of a comment (reads ,ignores and goes to the next line
					following_char_function()
				
				case else
					printf(OUT, "error in skip_comments_spaces_function")
			end switch
		end if
	end while
end procedure
--checks if /* appeared  on the file and */ appears too, ignores anything between them
procedure skip_comments_function()  
  current = getc(jackFile) 
  last=0
  while (not (last = '*' and current = '/')) do
	last = current
	current = getc(jackFile)
  end while
  -- after finding */ read the next char
  following_char_function()
end procedure

---------------- skipWhiteSpace_function -------------------
procedure skipWhiteSpace_function() --9=tab
	while (current = ' ' or current = '\n' or current = 9) do
		current = getc(jackFile)
	end while
end procedure

-------------------------------------------
--Tokenizing
function TokenizingFile_function(sequence res, integer level=0) 
	integer lev
	printf(xmlFile , "<" & res[1] &">")
	
	if (sequence(res[2])and equal(res[2],{})) then
		printf(xmlFile , "\n")
		lev=level-1 --for the first time there will be no space
		for i=0 to lev do
			printf(xmlFile, "  ")--prints double space
		end for
		printf(xmlFile , "</" & res[1] & ">")
	elsif (atom(res[2][1])) then
		printf(xmlFile ,( " " & res[2] & " </" & res[1] & ">"))
	else --if res holds more than 1 value 
		for i=1 to length(res[2]) do
			printf(xmlFile , "\n")
			for j=0 to level do
				printf(xmlFile, "  ")--prints double space
			end for
			TokenizingFile_function(res[2][i], level+1 )
		end for
		printf(xmlFile , "\n")
		lev=level-1 --for the first time there will be no space
		for i=0 to lev do
			printf(xmlFile, "  ")--prints double space
		end for
		printf(xmlFile , "</" & res[1] & ">")
	end if	
	return 0
end function

--parsing
procedure parsing_function(sequence res)  
	printf(TxmlFile , "<tokens>\n")
	Txml_printing_function(res)
	printf(TxmlFile , "</tokens>")
end procedure

procedure Txml_printing_function(sequence res)  
	if (sequence(res[2])and equal(res[2],{})) then
	elsif (atom(res[2][1])) then
		printf(TxmlFile ,( "<" & res[1] &"> " & res[2] & " </" & res[1] & ">\n"))
	else
		for i=1 to length(res[2]) do
			Txml_printing_function(res[2][i])
		end for
	end if
	
end procedure

--returns a word
function word_function(sequence cond = symbolAscii)
  integer condition=not find(current, cond)
  sequence word = ""  
  while (condition) do
	word = word & current
	current = getc(jackFile)
	condition=not find(current, cond)
  end while
  
  skipWhiteSpace_function()
  return word
end function



-----------------------------------------------------------
---------------- Lexical elements ------------------------
-----------------------------------------------------------
-- keyword_constant
function keyword_constant_function(sequence w = "")
	if equal(w,"") then
		w = word_function()
	end if
return {"keyword", w}
end function
-- string_constant
function string_constant_function()
	following_char_function()	-- for '"'
	sequence data = {"stringConstant", word_function({'"'})}
	following_char_function()	-- for '"'
	return data	
end function
-- integer_constant
function integer_constant_function()
  integer condition=find(current, {48,49,50,51,52,53,54,55,56,57})
  sequence word = ""  
  while (condition) do
	word = word & current
	current = getc(jackFile)
	condition=find(current, {48,49,50,51,52,53,54,55,56,57})
  end while
  skipWhiteSpace_function()
	return {"integerConstant", word}
end function
--identifier
function identifier_function(sequence currentWord="")	
	if (equal(currentWord, "")) then
		currentWord = word_function()
	end if
	return {"identifier", currentWord}
end function

-----operators
--can have white space before the op
function op_function()
	data = {}
	switch current do
		case '+' then
			data = data & "+"
		case '-' then
			data = data & "-"
		case '*' then
			data = data & "*"
		case '/' then 
			data = data & "/"
		case '&' then
			data = data & "&amp;"	
		case '|' then
			data = data & "|"
		case '<' then
			data = data & "&lt;"
		case '>' then
			data = data & "&gt;"
		case '=' then
			data = data & "="				
		case else
			printf(OUT, "there is no such operator")
	end switch

	following_char_function()
	return {"symbol" ,data}
end function

--can have white space before the op
function unary_op_function()
	data = {}
	skipWhiteSpace_function()
		switch current do
			case '~' then
				data = data & "~"
			case '-' then
				data = data & "-"
			case else
				printf(OUT, "there is no such unary operator")
		end switch
		following_char_function()
	return {"symbol" , data}
end function
--------------------------------------------------
--statements
---let gets an identifier and if  not = is after it is an array
function let_Statement_function()
	sequence data = { }
	data = append(append(data,{"keyword", "let"}), identifier_function())	
	if (current = '[') then
		data = append(data, {"symbol","["})
		following_char_function()
		data = append(append(data, expression_function()), {"symbol","]"})
		following_char_function()
	end if
		data = append(data,{"symbol", "="})
		following_char_function()
		data = append(append(data, expression_function()),{"symbol", ";"})
		following_char_function()
return {"letStatement", data}
end function

--if_Statement
function if_Statement_function()
	sequence data = { }
	data = append(append(data,{"keyword", "if"}),{"symbol", "("})
	following_char_function()
	data = append(append(data, expression_function()),{"symbol", ")"})
	following_char_function()
	skip_comments_spaces_function() -- incase of comments in the if
	data = append(data,{"symbol", "{"})
	following_char_function()
	data = append(append(data, statements_function()),{"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function() -- incase of comments after the if
		if (equal(current,'e') and equal(word_function(),"else")) then
			data = append(append(data, {"keyword","else"}), {"symbol","{"})
			following_char_function()
			skip_comments_spaces_function() --incase of comments in the else
			data = append(append(data, statements_function()), {"symbol","}"})
			following_char_function()
			skip_comments_spaces_function() -- incase of comments after the else
		end if	
return {"ifStatement", data}
end function

------------------
----------------------
--------------------
-----------------------
-----------------------
------------------------
---------------- class_function -------------------
function class_function()
	sequence data = {}
	skip_comments_spaces_function()
	data = append(data,{"keyword", "class"})
	word_function()
	data = append(data, identifier_function())
	skip_comments_spaces_function()
	data = append(data, {"symbol", "{"})
	following_char_function()
	skip_comments_spaces_function()
	data = data & classVar_SubroutineDec_function()
	data = append(data, {"symbol", "}"})
	following_char_function()
	return {"class", data}
end function
-- classVar_SubroutineDec_function 
function classVar_SubroutineDec_function()
	sequence curWord = word_function()
	  integer condition= equal(curWord, "static" ) or equal(curWord, "field" )
	sequence data ={}
	while (condition) do
		data = append(data, class_var_dec_function(curWord))
		skip_comments_spaces_function()
		curWord = word_function()
		condition= equal(curWord, "static" ) or equal(curWord, "field" )
	end while
	
	
	while (current != '}') do
		data = append(data, subroutine_dec_function(curWord))
		skip_comments_spaces_function()
		curWord = word_function()
	end while
	
	return data
end function
--classVarDec responsible for variables' decleration
function class_var_dec_function(sequence curWord)
	sequence data = {}
	
	data = append(append(append(data,{"keyword", curWord}), type_function()), identifier_function()) --for the first id
		
	while (current != ';') do --if there is more than 1 id
		data = append(data, {"symbol", ","})
		following_char_function()
		data = append(data, identifier_function())
	end while
	
	data = append(data,{"symbol", ";"}) --the endl
	following_char_function()
	
	return {"classVarDec", data}
end function
-- type can be int, boolean, char
function type_function(sequence curWord="")
	
	if (equal(curWord, "")) then
		curWord = word_function()
	end if
	
	integer condition =equal(curWord, "int") or equal(curWord, "boolean") or equal(curWord, "char")
	
	if (condition) then
		data = {"keyword", curWord}
	else --if it is not a type it is an identifier
		data = {"identifier", curWord}
	end if
	
	return data
end function
-- subroutineDec decides what is the type of the function (void/regular type such as int etc.)	
function subroutine_dec_function(sequence curWord)
	sequence data = {}

	data = append(data,{"keyword", curWord})
	curWord = word_function()	
	
	if (not(equal(curWord ,"void"))) then
		data = append(data,type_function(curWord))
	else
		data = append(data,{"keyword", "void"})
	end if
	
	data = append(append(data,identifier_function()), {"symbol", "("})
	following_char_function()
	
	data = append(append(data,parameter_list_function()), {"symbol", ")"})
	following_char_function()
	data = append(data,subroutine_body_function())
	
	return {"subroutineDec", data}
end function
--parameter_list_function deals with a list of parameters 
function parameter_list_function()
	sequence data = {}
	
	if (current != ')') then --for the first parameter
		data = append(append(data,type_function()),identifier_function())
	
		while (current = ',') do -- in case of more than 1 parameter
			data = append(data, {"symbol", ","})
			following_char_function()
			data = append(append(data,type_function()),identifier_function())
			
		end while
	end if	
	return {"parameterList", data}
end function
-- subroutine_body_function has list of variables and statements
function subroutine_body_function()
	sequence data = {}
	skip_comments_spaces_function()
	data = append(data, {"symbol", "{"})
	following_char_function()
	skip_comments_spaces_function()
	
	while (current = 'v') do --the variables are all together
		data = append(data, var_dec_function())
	end while
	
	data = append(data, statements_function())
	skip_comments_spaces_function()
	data = append(data, {"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function()
	return {"subroutineBody", data}
end function
--var_dec_function responsible to handle variables' decleration
function var_dec_function()
	sequence data = {}
	
	data = append(append(append(data, {"keyword", word_function()}), type_function()), identifier_function())
	
	while (current != ';') do --more than 1 variable
		data = append(data, {"symbol", ","})
		following_char_function()
		data = append(data, identifier_function())
	end while
	
	data = append(data,{"symbol", ";"})	
	following_char_function()
	
	return {"varDec", data}
end function

--statements creates a list of statements 1 by 1
function statements_function()
	sequence data = { }
	skip_comments_spaces_function()
	integer condition =not (current = '}') -- there is only one option for a statement in the subroutineBody before }
	while ( condition ) do
		data = append(data, statement_function())
		skip_comments_spaces_function()
		condition =not (current = '}')
	end while

	return {"statements", data}
end function

--statement 
function statement_function()
	sequence data = {}
	sequence result = {}
	sequence word = word_function()
	switch word do
		case "let" then
			result =  let_Statement_function()
		case "while" then
			result =  while_statement_function()
		case "if" then
			result =  if_Statement_function()
		case "return" then
			result =  return_statement_function()	
		case "do" then
			result =  do_statement_function()			
		case else
			printf(OUT, "there is no such statement")
	end switch
	data = data &result
return data
end function



--while_statement_function
function while_statement_function()
	sequence data = {}
	data = append(append(data,{"keyword", "while"}),{"symbol", "("})
	-- word_function in statements_function made skipWhiteSpace_function

	following_char_function()
	data = append(append(data, expression_function()),{"symbol", ")"})
	following_char_function()
	skip_comments_spaces_function() -- if there is a comment inside the while (before{)

	data = append(data,{"symbol", "{"})
	skip_comments_spaces_function()-- if there is a comment inside the while (after{) 
	following_char_function()
	--statements_function() skips
	data = append(append(data, statements_function()),{"symbol", "}"})
	following_char_function()
	skip_comments_spaces_function() --if there is a comment after the while
	return {"whileStatement", data}
end function

--do_statement_function
function do_statement_function()
	sequence data = {}
	data = append(data,{"keyword", "do"})-- next char was read by word_function()
	data = append(data & subroutine_call_function(),{"symbol", ";"})
	following_char_function()		

return {"doStatement" ,  data}
end function

--return_statement_function
function return_statement_function()
	sequence data = {}
	data = append(data,{"keyword", "return"})	
	if (current != ';') then
		data = append(data, expression_function())
	end if
	data = append(data,{"symbol", ";"})
	following_char_function()		
return {"returnStatement" , data}
end function


-- Expressions 
-- expression_function 
function expression_function()
	sequence data = {}
	data = append(data,term_function())
	integer condition =not ( current = ')' or current = ',' or current = ';' or current = ']' )
	while (condition) do
		data = append(append(data, op_function()), term_function())
		condition =not ( current = ')' or current = ',' or current = ';' or current = ']' )
	end while
return {"expression", data}
end function

--term_function
function term_function()
	sequence data = {}, word
	--parameters and ops
	if( current = '~' or current = '-' ) then 
		data = append(append(data, unary_op_function()), term_function())
	elsif(current >= 48 and current <= 57)then
		data = append(data, integer_constant_function())
	elsif(current = '"')then
		data = append(data, string_constant_function())
	elsif(current = '(')then
		data = append(data, {"symbol", "("})
		following_char_function()
		data = append(append(data, expression_function()),{"symbol", ")"})
		following_char_function()
	else
		word = word_function()
		if(find (word,keywordConstant)) then
			data = append(data, keyword_constant_function(word))
		elsif (current = '.') or (current = '(') then --function
			data = data & subroutine_call_function(word)
		elsif(current = '[')then --array
			data = append(append(data,identifier_function(word)),{"symbol", "["})
			following_char_function()
			data = append(append(data, expression_function()),{"symbol", "]"})
			following_char_function()
		else
			data = append(data,identifier_function(word))
		end if
	end if
	
return {"term", data}
end function

--subroutine_call_function 
function subroutine_call_function(sequence word = "")
	if equal(word,"") then
		word = word_function()
	end if
	sequence data = {identifier_function(word)}
	if(current = '.') then
		data = append(data,{"symbol", "."})
		following_char_function()
		data = append(data, identifier_function())
	end if

		data = append(data,{"symbol", "("})
		following_char_function()
		data = append(append(data, expression_list_function()),{"symbol", ")"})
		following_char_function()
	return data
end function

-- expression_list_function 
function expression_list_function()
	sequence data = {}
	if( not (current = ')')) then
		data = append(data, expression_function())
		while (not (current = ')')) do --more than 1 express
			data = append(data, {"symbol", ","})
			following_char_function()
			data = append(data, expression_function())
		end while
	end if
return {"expressionList", data}
end function