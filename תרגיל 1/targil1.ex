--Hodaya Elimelech 318627247
--Yael Malka 316151406

--goes to C:\Users\yael\euphoria

include std/filesys.e -- for walk_dir
include std/wildcard.e -- for is_match
include std/console.e	-- for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
with trace --for trace

--Main Program
integer is_vm --to check if the file is a vm file
constant TRUE = 1,FALSE = 0
constant IN = 0,OUT = 1
constant EOF = -1
sequence path
sequence tempFile 
object exit_code
atom counter = 0
path = prompt_string ("Enter full path to be used:\n")
exit_code = walk_dir(path, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "The folder you chose doesn't exist\n")
else
	printf(OUT, "Executed Successfully\n")
end if
--------------------------------------
-- goes through the files in the chosen folder
function look_at(sequence path_name, sequence item) 
	integer asmFile
	integer vmFile
	object line
	
	is_vm = is_match("*.vm", item[D_NAME])	
	
	if is_vm then
		--creates an asm file as a translation to the vm file, accesses to the first matching file and works on it
		tempFile = split(item[D_NAME],'.')
		asmFile = open( path_name & "\\" & tempFile[1] & ".asm", "w")--for writing
		sequence fullpath = path_name&"\\"&item[D_NAME]		
		vmFile = open(fullpath, "r")--for reading 
		
		if vmFile = -1 then
			printf(1, "Cannot open file at %s\n", {fullpath})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
		if (asmFile) = -1  then
				printf(1, "Can't open file %s\n", {path_name & "\\" & tempFile[1] & ".asm"})
			any_key ("\n   Press any key to continue... ")--escape
		end if 
	while sequence(line) entry do			-- end while when line is equal to -1
		sequence parts_of_line
		parts_of_line = split_any(line, " \n")	
		
		switch parts_of_line[1] do --the function that needs to operate
			case "pop" then
				pop_function(parts_of_line, asmFile)
			case "push" then
				push_function(parts_of_line, asmFile)
			case "not" then
				not_function(parts_of_line, asmFile)				
			case "or" then
				or_function(parts_of_line, asmFile)
			case "and" then
				and_function(parts_of_line, asmFile)
			case "eq" then
				equal_function(parts_of_line, asmFile)
			case "gt" then
				greater_than_function(parts_of_line, asmFile)
			case "lt" then
				less_than_function(parts_of_line, asmFile)
			case "add" then
				add_function(parts_of_line, asmFile)
			case "sub" then
				sub_function(parts_of_line, asmFile)
			case "neg" then
				negative_function(parts_of_line, asmFile)	
		end switch
		entry
		line = gets(vmFile) -- reads the next value to operate on
	end while		
		close(vmFile)
		close(asmFile)
	end if
	return 0 --function must return a value it's like a procedure when returning 0		
end function
---------------------------------------------------
--pop
procedure popToD(integer asmFile)
		printf(asmFile, "%s%s%s%s", {"@SP\n","M=M-1\n","A=M\n","D=M\n"})
end procedure

procedure popToA(integer asmFile)
		printf(asmFile, "%s%s%s%s", {"@SP\n","M=M-1\n","A=M\n","A=M\n"})
end procedure
-- D=M+D
function pop_M_and_D_to_D(integer asmFile, object offset, object segment)
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nD=M+D\n@13\nM=D\n"
	printf(asmFile, "%s", {temp})
	popToD(asmFile)
	printf(asmFile, "%s", {"@13\nA=M\nM=D\n"})
	return 0
end function

-- D=A+D
function pop_A_and_D_to_D(integer asmFile, object offset, object segment)
	sequence temp ="@" & offset & "\nD=A\n@" & segment & "\nD=A+D" & "\n@13" & "\nM=D\n"
	printf(asmFile, "%s", {temp})
	popToD(asmFile)
	printf(asmFile, "%s", {"@13\nA=M\nM=D\n"})
	return 0
end function
----
procedure pop_function(sequence parts_of_line, integer asmFile)
	switch parts_of_line[2] do -- parts_of_line[2] is the segment in the memory parts_of_line[3] is the value
		case "local" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "LCL")
			break
		case "argument" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "ARG")
			break
		case "this" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "THIS ")
			break
		case "that" then
			pop_M_and_D_to_D(asmFile, parts_of_line[3], "THAT ")
			break
		case "temp" then
			pop_A_and_D_to_D(asmFile, parts_of_line[3], "5")		
			break
		case "pointer" then
			pop_A_and_D_to_D(asmFile, parts_of_line[3], "THIS")
			break
		case "static" then			
			popToD(asmFile)
			sequence final="@" & tempFile[1] & "." & parts_of_line[3] & "\nM=D\n"
			printf(asmFile, "%s", {final})
			break
		case else
			printf(asmFile, "%s", {"ERROR!!! there is no such segment"})
			puts(1,"ERROR!!! there is no such segment\n PROBLEM in the vm file")
			abort(1)
	end switch			
end procedure
-- A=M+D
procedure push_M_and_D_to_A(integer asmFile, object offset, object segment)	
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nA=M+D\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"
	printf(asmFile, "%s", {temp})
end procedure

-- A=A+D
procedure push_A_and_D_to_A(integer asmFile, object offset, object segment)	
	sequence temp="@" & offset & "\nD=A\n@" & segment & "\nA=A+D\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"
	printf(asmFile, "%s", {temp})
end procedure
-----
procedure push_function(sequence parts_of_line, integer asmFile)
	switch parts_of_line[2] do
		case "local" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "LCL")
			break
		case "argument" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "ARG")
			break
		case "this" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "THIS")
			break
		case "that" then
			push_M_and_D_to_A(asmFile, parts_of_line[3], "THAT")
			break
		case "temp" then
			push_A_and_D_to_A(asmFile, parts_of_line[3], "5")		
			break
		case "pointer" then
			push_A_and_D_to_A(asmFile, parts_of_line[3], "THIS")
			break
		case "static" then
			sequence final = "@" &tempFile[1] & "." & parts_of_line[3] & "\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"		
			printf(asmFile, "%s", {final})
			break
		case "constant" then
			printf(asmFile, "%s", {"@"& parts_of_line[3] &"\nD=A\n@SP\nA=M\nM=D\n@SP\nM=M+1\n"})
			break
		case else
			printf(asmFile, "%s", {"ERROR!!! there is no such segment"})
			puts(1,"ERROR!!! there is no such segment\n PROBLEM in the vm file")
			abort(1)
	end switch
end procedure
----add,subtract,negative
procedure add_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=M+D\n"})
end procedure

procedure sub_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=M-D\n"})
end procedure

procedure negative_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	printf(asmFile, "%s%s%s%s%s", {"@SP\n","A=M\n","M=-D\n","@SP\n","M=M+1\n"})
end procedure
----equal operators
procedure equal_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	popToA(asmFile)
	printf(asmFile, "%s%d%s", {"D=D-A\n@IF_TRUE",counter,"\nD; JEQ\n@SP\nA=M\nM=0\n@end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE",counter,")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure
procedure greater_than_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	popToA(asmFile)
	printf(asmFile, "%s%d%s%d", {"D=A-D\n@IF_TRUE",counter,"\nD; JGT\n@SP\nA=M\nM=0\n@end",counter+1})	
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE",counter,")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure

procedure less_than_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	popToA(asmFile)
	printf(asmFile, "%s%d%s", {"D=A-D\n@IF_TRUE",counter,"\nD; JLT\n@SP\nA=M\nM=0\n@end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s%d%s", {"\n0; JMP\n(IF_TRUE" , counter , ")\n@SP\nA=M\nM=-1\n(end"})
	printf(asmFile, "%d" , {counter+1})
	printf(asmFile, "%s", {")\n@SP\nM=M+1\n"})
	counter = counter + 2
end procedure
-----logical operators
procedure and_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=D&M\n"})
end procedure

procedure or_function(sequence parts_of_line, integer asmFile)
	popToD(asmFile)
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=D|M\n"})
end procedure

procedure not_function(sequence parts_of_line, integer asmFile)
	printf(asmFile, "%s%s%s", {"@SP\n","A=M-1\n","M=!M\n"})
end procedure