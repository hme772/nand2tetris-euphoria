include std/filesys.e -- needed for walk_dir
include std/wildcard.e -- needed for is_match
include std/console.e	-- needed for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
with trace

constant IN = 0, OUT = 1, FALSE = 0, TRUE = 1, EOF = -1
integer ok
sequence answer, filenameTmp
object exit_code
atom counter = 0
sequence keyword = {"class","constructor","function","method","field","static","var","int","char","boolean","void","true","false","null","this","let","do","if","else","while","return"}
sequence symbol = "{}()[].,;+-*/&|<>=~}"
sequence symbolAscii = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~',' ','\t','"','\n'}
sequence keywordConstant={"true","false","null","this"}
------ walk dir vars
sequence fullpath, xmlpath, Txmlpath, fName
integer fn_jack, fn_vm, fn_xml, fn_Txml
object line
sequence splitLine, db
integer current


-----------------------------------------------------------
-------------------- print Tokenizing - xml ---------------
-----------------------------------------------------------
procedure TokenizingXML(sequence res, integer level=0)  
			if sequence(res) and equal(res ,{}) then		-- do nothing
			
			elsif isString(res) then							-- for string value
				printf(fn_xml , res)
			else
				if isString(res[1]) then						-- for tag and sequence values
					printf(fn_xml , "<" & res[1] &">")
					
					if not(isString(res[2])) then 
						printf(fn_xml , "\n")
						--printTabFunc(level)
					end if
					
						for i=2 to length(res) do			
							TokenizingXML(res[i], level+1)
						end for

					printf(fn_xml , "<" & "/" & res[1] & ">\n")
					--printTabFunc(level)
				else												-- for sequence value
						for i=1 to length(res) do			
							TokenizingXML(res[i], level+1)
						end for				
				end if
			end if	
end procedure

function isString(sequence s)  
	integer str = 0
	if sequence(s) and equal(s ,{}) then
		str = 0
	elsif sequence(s) and atom(s[1]) then
		str = 1
	end if
	return str
end function

-----------------------------------------------------------
-------------------- print Tokenizing - xml ---------------
-----------------------------------------------------------
procedure oldTokenizingPrintFunc(sequence res, integer level=0)  
	printf(fn_xml , "<" & res[1] &">")
	
	if (sequence(res[2])and equal(res[2],{})) then
		printf(fn_xml , "\n")
		printTabFunc(level-1)
		printf(fn_xml , "<" & "/" & res[1] & ">")
	elsif (atom(res[2][1])) then
		printf(fn_xml ,( " " & res[2] & " "))
		printf(fn_xml , "<" & "/" & res[1] & ">")
	else
		for i=1 to length(res[2]) do
			printf(fn_xml , "\n")
			printTabFunc(level)
			oldTokenizingPrintFunc(res[2][i], level+1 )
		end for
		printf(fn_xml , "\n")
		printTabFunc(level-1)
		printf(fn_xml , "<" & "/" & res[1] & ">")
	end if	
end procedure

procedure printTabFunc(integer level)
	for i=0 to level do
		printf(fn_xml, 32 & 32)
	end for
end procedure

-----------------------------------------------------------
------------- print Parsing - T.xml -----------------------
-----------------------------------------------------------
procedure ParsingTXML(sequence res)  
	printf(fn_Txml , "<" & "tokens" &">\n")
		TxmlPrintFunc(res)
	printf(fn_Txml , "<" & "/" & "tokens" & ">")
end procedure

procedure TxmlPrintFunc(sequence res)  
	if (sequence(res[2])and equal(res[2],{})) then
	elsif (atom(res[2][1])) then
		printf(fn_Txml , "<" & res[1] &">")
		printf(fn_Txml ,( " " & res[2] & " "))
		printf(fn_Txml , "<" & "/" & res[1] & ">\n")
	else
		for i=1 to length(res[2]) do
			TxmlPrintFunc(res[2][i])
		end for
	end if
	
end procedure

-----------------------------------------------------------
------------------- internal func -------------------------
-----------------------------------------------------------


---------------- skipNote ------------------- checked!
-- if /* was readed, skip until */
procedure skipNote()  
  integer last = 0
  current = getc(fn_jack)
  
  while (not (last = '*' and current = '/')) do
	last = current
	current = getc(fn_jack)
  end while
  -- after finding */ read the next char
  nextChar()
end procedure

---------------- skipSpaceEnterTab -------------------
procedure skipSpaceEnterTab()
	while (current = ' ' or current = '\n' or current = 9) do
		current = getc(fn_jack)
	end while
end procedure

------------------ skip ------------------------- checked!
-- skip on: space , // ... , /* ... */ , \n
procedure skip() 
	while (current = '/' or current = ' ' or current = '\n' or current = 9) do
		skipSpaceEnterTab()
		if current = '/' then
			nextChar()
			switch current do
				-- for: //
				case '/' then
					gets(fn_jack)
					nextChar()
				-- for: /*
				case '*' then
					skipNote()
				case else
					printf(OUT, "error in skip")
			end switch
		end if
	end while
end procedure

---------------- nextChar -------------------
procedure nextChar()
	current = getc(fn_jack)
	skipSpaceEnterTab()
end procedure

---------------------- readWord --------------------- checked!
function readWord(sequence cond = symbolAscii)
  sequence word = ""  
  
  while (not find(current, cond)) do
	word = word & current
	current = getc(fn_jack)
  end while
  
  skipSpaceEnterTab()
  return word
end function



-----------------------------------------------------------
---------------- Lexical elements ------------------------
-----------------------------------------------------------

function StringConstantFunc()
	nextChar()	-- for '"'
	sequence db = {"stringConstant", readWord({'"'})}
	nextChar()	-- for '"'
	return db	
end function

function integerConstantFunc()
	sequence word = ""  
  
  while (find(current, {48,49,50,51,52,53,54,55,56,57})) do
	word = word & current
	current = getc(fn_jack)
  end while
  
  skipSpaceEnterTab()
	return {"integerConstant", word}
end function

function identifierFunc(sequence curWord="")	
	if (equal(curWord, "")) then
		curWord = readWord()
	end if
	return {"identifier", curWord}
end function

-----------------------------------------------------------
---------------- Program structure ------------------------
-----------------------------------------------------------

---------------- classFunc -------------------
function classFunc()
	sequence db = {}
	skip()
	db = append(db,{"keyword", "class"})
	readWord()
	db = append(db, classNameFunc())
	skip()
	db = append(db, {"symbol", "{"})
	nextChar()
	skip()
	db = db & classVarAndSubroutineDec()
	db = append(db, {"symbol", "}"})
	nextChar()
	return {"class", db}
end function
-------------- classVarAndSubroutineDec ------------
function classVarAndSubroutineDec()
	sequence db = {}
	sequence curWord = readWord()

	while (equal(curWord, "field") or equal(curWord, "static")) do
		db = append(db, classVarDecFunc(curWord))
		skip()
		curWord = readWord()
	end while
	
	while (current != '}') do
		db = append(db, subroutineDecFunc(curWord))
		skip()
		curWord = readWord()
	end while
	
	return db
end function
---------------- classVarDec -------------------
function classVarDecFunc(sequence curWord)
	sequence db = {}
	
	db = append(db,{"keyword", curWord})
	db = append(db, typeFunc())
	db = append(db, varNameFunc())
		
	while (current != ';') do
		db = append(db, {"symbol", ","})
		nextChar()
		db = append(db, varNameFunc())
	end while
	
	db = append(db,{"symbol", ";"})
	nextChar()
	
	return {"classVarDec", db}
end function
--------------------- type -----------------------
function typeFunc(sequence curWord="")
	
	if (equal(curWord, "")) then
		curWord = readWord()
	end if
	
	if (equal(curWord, "int") or equal(curWord, "char") or equal(curWord, "boolean")) then
		db = {"keyword", curWord}
	else
		db = {"identifier", curWord}
	end if
	
	return db
end function
---------------- subroutineDec -------------------	
function subroutineDecFunc(sequence curWord)
	sequence db = {}

	db = append(db,{"keyword", curWord})
	curWord = readWord()	
	if (equal(curWord, "void")) then
		db = append(db,{"keyword", "void"})
	else
		db = append(db,typeFunc(curWord))
	end if
	
	db = append(db,subroutineNameFunc())
	db = append(db, {"symbol", "("})
	nextChar()
	
	db = append(db,parameterListFunc())
	
	db = append(db, {"symbol", ")"})
	nextChar()
	db = append(db,subroutineBodyFunc())
	
	return {"subroutineDec", db}
end function
------------ parameterList -------------
function parameterListFunc()
	sequence db = {}
	
	if (current != ')') then
		db = append(db,typeFunc())
		db = append(db,varNameFunc())

		while (current = ',') do
			db = append(db, {"symbol", ","})
			nextChar()
			db = append(db,typeFunc())
			db = append(db,varNameFunc())
		end while
	end if
	
	return {"parameterList", db}
end function
------------ subroutineBody -------------
function subroutineBodyFunc()
	sequence db = {}
	skip()
	db = append(db, {"symbol", "{"})
	nextChar()
	skip()
	
	while (current = 'v') do
		db = append(db, varDecFunc())
	end while
	
	db = append(db, statementsFunc())
	skip()
	db = append(db, {"symbol", "}"})
	nextChar()
	skip()
	return {"subroutineBody", db}
end function
------------ varDec -------------
function varDecFunc()
	sequence db = {}
	
	db = append(db, {"keyword", readWord()})
	db = append(db, typeFunc())
	db = append(db, varNameFunc())
	
	while (current != ';') do
		db = append(db, {"symbol", ","})
		nextChar()
		db = append(db, varNameFunc())
	end while
	
	db = append(db,{"symbol", ";"})	
	nextChar()
	
	return {"varDec", db}
end function

------------ className -------------
function classNameFunc()
	return identifierFunc()
end function
--------- subroutineName -----------
function subroutineNameFunc()
	return identifierFunc()
end function
----------- varName ---------------
function varNameFunc()
	return identifierFunc()
end function


-----------------------------------------------------------
------------------- Statements ---------------------------
-----------------------------------------------------------

---------------------statements------------------------
function statementsFunc()
	sequence db = { }
	skip()
	-- there is only one option to statement - in subroutineBody, before }
	while (not (current = '}')) do
		db = append(db, statementFunc())
		skip()
	end while

	return {"statements", db}
end function

-----------------------statement-------------------------
function statementFunc()
	sequence db = {}
	sequence word = readWord()
	switch word do
		case "let" then
			db = db & letStatementFunc()
		case "if" then
			db = db & ifStatementFunc()
		case "while" then
			db = db & whileStatementFunc()
		case "do" then
			db = db & doStatementFunc()
		case "return" then
			db = db & returnStatementFunc()				
		case else
			printf(OUT, "error in statementFunc")
	end switch
	
return db
end function

------------------------letStatementFunc------------------------
function letStatementFunc()
	sequence db = { }
	db = append(db,{"keyword", "let"})
	-- next char was read by readWord()
		

	db = append(db, varNameFunc())
	
	if not (current = '=') then
		db = append(db, {"symbol","["})
		nextChar()
		
		db = append(db, expressionFunc())
		
		db = append(db, {"symbol","]"})
		nextChar()
	end if
	
	db = append(db,{"symbol", "="})
	nextChar()
	
	db = append(db, expressionFunc())
	
	db = append(db,{"symbol", ";"})
	nextChar()

return {"letStatement", db}
end function

-----------------------ifStatementFunc-------------------------
function ifStatementFunc()
	sequence db = { }
	db = append(db,{"keyword", "if"})
	-- readWord in statementsFunc made skipSpaceEnterTab

	db = append(db,{"symbol", "("})
	nextChar()

	db = append(db, expressionFunc())

	db = append(db,{"symbol", ")"})
	nextChar()
	skip() -- for: if (true) /* ... */  or if (true) //

	db = append(db,{"symbol", "{"})
	nextChar()
	--statementsFunc() doing skip

	db = append(db, statementsFunc())

	db = append(db,{"symbol", "}"})
	nextChar()
	skip() -- for: if(){} /* ... */  if(){} //
	if (current = 'e') then
		if equal(readWord(),"else") then
			db = append(db, {"keyword","else"})
			db = append(db, {"symbol","{"})
			nextChar()
			
			db = append(db, statementsFunc())
			
			db = append(db, {"symbol","}"})
			nextChar()
			skip() -- for: else{} /* ... */  else{} //
		end if
	end if
	
return {"ifStatement", db}
end function

-------------------------whileStatementFunc-----------------------
function whileStatementFunc()
	sequence db = {}
	db = append(db,{"keyword", "while"})
	-- readWord in statementsFunc made skipSpaceEnterTab

	db = append(db,{"symbol", "("})
	nextChar()

	db = append(db, expressionFunc())

	db = append(db,{"symbol", ")"})
	nextChar()
	skip() -- for: while (true) /* ... */  or while (true) //

	db = append(db,{"symbol", "{"})
	nextChar()
	--statementsFunc() doing skip

	db = append(db, statementsFunc())

	db = append(db,{"symbol", "}"})
	nextChar()
	skip() -- for: if(){} /* ... */  if(){} //
	return {"whileStatement", db}
end function

-----------------------doStatementFunc-------------------------
function doStatementFunc()
	sequence db = {}
	db = append(db,{"keyword", "do"})
	-- next char was read by readWord()
	
	db = db & subroutineCallFunc()	
	
	db = append(db,{"symbol", ";"})
	nextChar()		

return {"doStatement" ,  db}
end function

---------------------------returnStatementFunc---------------------
function returnStatementFunc()
	sequence db = {}
	db = append(db,{"keyword", "return"})
	-- next char was read by readWord()
	
	if not (current = ';') then
		db = append(db, expressionFunc())
	end if
	
	db = append(db,{"symbol", ";"})
	nextChar()		

return {"returnStatement" , db}
end function

-----------------------------------------------------------
------------------- Expressions ---------------------------
-----------------------------------------------------------

---------------------- expressionFunc --------------------------
function expressionFunc()
sequence db = {}
db = append(db,termFunc())

while not (current = ';' or current = ')' or current = ']' or current = ',') do
		db = append(db, opFunc())
		db = append(db, termFunc())
end while

return {"expression", db}
end function

---------------------- termFunc --------------------------
function termFunc()
	sequence db = {}, word

	if(current = '-' or current = '~') then
		db = append(db, unaryOpFunc())
		db = append(db, termFunc())
	elsif(current >= 48 and current <= 57)then
		db = append(db, integerConstantFunc())
	elsif(current = '"')then
		db = append(db, StringConstantFunc())
	elsif(current = '(')then
		db = append(db, {"symbol", "("})
		nextChar()
		db = append(db, expressionFunc())
		db = append(db,{"symbol", ")"})
		nextChar()
	else
		word = readWord()
		if(find (word,keywordConstant)) then
			db = append(db, keywordConstantFunc(word))
		elsif(current = '(') or (current = '.') then
			db = db & subroutineCallFunc(word)
		elsif(current = '[')then
			db = append(db,identifierFunc(word))
			db = append(db,{"symbol", "["})
			nextChar()
			db = append(db, expressionFunc())
			db = append(db,{"symbol", "]"})
			nextChar()
		else
			db = append(db,identifierFunc(word))
		end if
	end if
	
return {"term", db}
end function

---------------------- subroutineCallFunc --------------------------
function subroutineCallFunc(sequence word = "")
	if equal(word,"") then
		word = readWord()
	end if
	
	sequence db = {identifierFunc(word)}
	if(current = '.') then
		db = append(db,{"symbol", "."})
		nextChar()
		db = append(db, identifierFunc())
	end if

	db = append(db,{"symbol", "("})
		nextChar()
		db = append(db, expressionListFunc())
		db = append(db,{"symbol", ")"})
		nextChar()
	return db
end function

---------------------- expressionListFunc --------------------------
function expressionListFunc()
	sequence db = {}
	if( not (current = ')')) then
		db = append(db, expressionFunc())
		while not (current = ')') do
			db = append(db, {"symbol", ","})
			nextChar()
			db = append(db, expressionFunc())
		end while
	end if
return {"expressionList", db}
end function

---------------------- opFunc --------------------------
-- only space can be before the operator.
-- read the next char when finished
function opFunc()
sequence db = {}
	switch current do
		case '+' then
			db = db & "+"
		case '-' then
			db = db & "-"
		case '*' then
			db = db & "*"
		case '/' then 
			db = db & "/"
		case '&' then
			db = db & "&amp;"	
		case '|' then
			db = db & "|"
		case '<' then
			db = db & "&lt;"
		case '>' then
			db = db & "&gt;"
		case '=' then
			db = db & "="				
		case else
			printf(OUT, "misstake in opFunc()")
	end switch

-- read the next char
nextChar()
return {"symbol" ,db}
end function

---------------------- unaryOpFunc --------------------------
-- only space can be before the operator.
-- read the next char when finished
function unaryOpFunc()
sequence db = {}
skipSpaceEnterTab()
	switch current do
		case '-' then
			db = db & "-"
		case '~' then
			db = db & "~"
		case else
			trace(1)
			printf(OUT, "misstake in unaryOpFunc()")
	end switch

-- read the next char
nextChar()
return {"symbol" , db}
end function

---------------------- keywordConstantFunc --------------------------
function keywordConstantFunc(sequence word = "")
	if equal(word,"") then
		word = readWord()
	end if
return {"keyword", word}
end function


-----------------------------------------------------------
-------------------------- MAIN ---------------------------
-----------------------------------------------------------


-------------------------------------------------
------ walk_dir function------
-- will go throug all the files in the folder user enterd

function look_at(sequence path_name, sequence item) --  this is going to work on every file  

  ok = is_match("*.jack", item[D_NAME])  
  
  if ok then    -- if this file is ok
    fullpath = path_name&"\\"&item[D_NAME] -- build the path to the file. item has a few elements in it, we just need the first 1 that has the file name
    fn_jack = open(fullpath, "r")  
    filenameTmp = split(item[D_NAME],'.')
    -- fn_vm = open( path_name & "\\" & filenameTmp[1] & ".vm", "w")     
    fn_xml = open( path_name & "\\" & filenameTmp[1] & "Our" & ".xml", "w")
    fn_Txml = open( path_name & "\\" & filenameTmp[1] & "TOur" & ".xml", "w")
    
    if fn_jack = -1 then
      printf(1, "Can't open file %s\n", {fullpath})
      abort(1)
    end if 
    if (fn_xml) = -1  then
        printf(1, "Can't open file %s\n", {path_name&"\\hello.asm"})
        abort(1)
    end if 
    if fn_Txml = -1 then
      printf(1, "Can't open file %s\n", {fullpath})
      abort(1)
    end if  
	
	nextChar()
    sequence db = classFunc()
	trace(1)
	trace(0)
	--TokenizingXML(db)
	oldTokenizingPrintFunc(db)
	ParsingTXML(db)	
	
    -- close(fn_vm)
    close(fn_jack)
    close(fn_xml)
    close(fn_Txml)

  end if
  return 0    
  
end function


--------Code stars here:--------

answer = prompt_string ("enter full path name to use:\n")
exit_code = walk_dir(answer, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "Folder doesn't exists\n")
else
	printf(OUT, "Executed\n")
end if

any_key ("\n   Press any key to close this Window... ")