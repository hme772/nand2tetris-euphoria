-- mordechai rothkoff 302888300
-- elad de-roos 305689408

include std/filesys.e -- needed for walk_dir
include std/wildcard.e -- needed for is_match
include std/console.e	-- needed for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
with trace
-- address to use:
-- C:\Euphoria\bin\programs

constant IN = 0, OUT = 1, FALSE = 0, TRUE = 1, EOF = -1
integer ok
sequence answer, filenameTmp
object exit_code
atom counter = 0
sequence keyword = {"class","constructor","function","method","field","static","var","int","char","boolean","void","true","false","null","this","let","do","if","else","while","return"}
sequence symbol = {"{","}","(",")","[","]",".",",",";","+","-","*","/","&","|","<",">","=","~"}
sequence symbolAscii = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~',' '}
object symbolToSplit = " {}()[].,;+-*/&|<>=~"
------ walk dir vars
sequence fullpath
integer fn_jack, fn_vm, fn_xml
object line
sequence splitLine
integer current

--------------------------------------------------

function classFunc()
	
	sequence db = {"class", {{keyword, "class"}, readIdentifier(), {symbol, '{'},  classVarDec(),subroutineDec(),{symbol, "}"}}	
	db[2][2] = 	
	verify("{")		
	db[2][4] =
	db[2][5] = 
	verify("}")	
	return db
end function


function readIdentifier()
	sequence db={"identifier", readWord()}
--	db[2]=readWord()
	return db
end function


function classVarDec()
	sequence db
	switch current do
		case "//" then
			nextLine()
		case "/*" then
			noteFunc()
		case "field" then
			db = {"classVarDec", {{keyword, "field"}, {},{},{}, {";"}}	
		case "static" then				
			db = {"classVarDec", {{keyword, "static"}, {},{},{}, {";"}}
		case else
			db={}
		end switch
	
	return db
end function

	
function subroutineDec()

end function

function ()

end function

procedure skipNote()  
  integer last = 0
  current = getc(fn_jack)
  
  while (not (last = '*' and current = '/')) do
  last = current
  current = getc(fn_jack)
  end while  
end procedure

	
procedure nextLine()
	gets(fn_jack)
	current = getc(fn_jack)
end procedure


function readWord()
	while (current = ' ') do
		current = getc(fn_jack)
	end while  
  sequence word = ""  
  
  while (not find(current, symbolAscii)) do
    word = word & current
	current = getc(fn_jack)
  end while
  
  return word
end function


function verify(sequence token)
	while (current = ' ') do
		current = getc(fn_jack)
	end while  
	
	if ((current = token) and find(token, symbolAscii))
		current = getc(fn_jack)
	end if
	return {symbol, current}		-- TODO string
end function



----------- print function: ------------------

procedure printOpenTAG(sequence toPrint)
	printf(xml_fn, "<" & toPrint & ">")
end procedure

procedure printCloseTAG(sequence toPrint)
	printf(xml_fn, "</" & toPrint & ">")
end procedure

function lexicalElement()
	find( ,keyword)	--TODO
end function


-------------------------------------------------
------ walk_dir function------
-- will go throug all the files in the folder user enterd

function look_at(sequence path_name, sequence item) -- this is going to work on every file	

	ok = is_match("*.jack", item[D_NAME])	
	
	if ok then	  -- if this file is ok
		fullpath = path_name&"\\"&item[D_NAME] -- build the path to the file. item has a few elements in it, we just need the first 1 that has the file name
		fn_jack = open(fullpath, "r")	
		filenameTmp = split(item[D_NAME],'.')
		--fn_vm = open( path_name & "\\" & filenameTmp[1] & ".vm", "w") 		
		fn_xml = open( path_name & "\\" & filenameTmp[1] & ".xml", "w")
		
		if fn_jack = -1 then
			printf(1, "Can't open file %s\n", {fullpath})
			abort(1)
		end if 
		if (fn_xml) = -1  then
				printf(1, "Can't open file %s\n", {path_name&"\\hello.asm"})
				abort(1)
		end if 

		
		getc(fn_jack)
		sequence db = classFunc()

		close(fn_vm)
		close(fn_jack)

	end if
	return 0		
	
end function


--------Code stars here:--------

answer = prompt_string ("enter full path name to use:\n")
exit_code = walk_dir(answer, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "Folder doesn't exists\n")
else
	printf(OUT, "Executed\n")
end if

any_key ("\n   Press any key to close this Window... ")


