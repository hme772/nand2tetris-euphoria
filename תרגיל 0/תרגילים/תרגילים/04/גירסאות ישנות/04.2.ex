-- mordechai rothkoff 302888300
-- elad de-roos 305689408

include std/filesys.e -- needed for walk_dir
include std/wildcard.e -- needed for is_match
include std/console.e	-- needed for prompt_string	
include std/sequence.e  -- for split
include std/text.e -- for switch
with trace
-- address to use:
-- C:\Euphoria\bin\programs

constant IN = 0, OUT = 1, FALSE = 0, TRUE = 1, EOF = -1
integer ok
sequence answer, filenameTmp
object exit_code
atom counter = 0
sequence keyword = {"class","constructor","function","method","field","static","var","int","char","boolean","void","true","false","null","this","let","do","if","else","while","return"}
sequence symbol = {"{","}","(",")","[","]",".",",",";","+","-","*","/","&","|","<",">","=","~"}
sequence symbolAscii = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~',' '}
object symbolToSplit = " {}()[].,;+-*/&|<>=~"
------ walk dir vars
sequence fullpath
integer fn_jack, fn_vm, fn_xml, fn_Txml
object line
sequence splitLine
integer current





--------------------------------------------------------------
---------------------- walk_dir function----------------------
-- will go throug all the files in the folder user enterd-----
--------------------------------------------------------------

function look_at(sequence path_name, sequence item) -- this is going to work on every file	

	ok = is_match("*.jack", item[D_NAME])	
	
	if ok then	  -- if this file is ok
		fullpath = path_name&"\\"&item[D_NAME] -- build the path to the file. item has a few elements in it, we just need the first 1 that has the file name
		fn_jack = open(fullpath, "r")	
		filenameTmp = split(item[D_NAME],'.')
		--fn_vm = open( path_name & "\\" & filenameTmp[1] & ".vm", "w") 		
		fn_xml = open( path_name & "\\" & filenameTmp[1] & "Our" & ".xml", "w")
		fn_Txml = open( path_name & "\\" & filenameTmp[1] & "TOur" & ".xml", "w")
		
		if fn_jack = -1 then
			printf(1, "Can't open file %s\n", {fullpath})
			abort(1)
		end if 
		if (fn_xml) = -1  then
				printf(1, "Can't open file %s\n", {path_name&"\\hello.asm"})
				abort(1)
		end if 
		if fn_Txml = -1 then
			printf(1, "Can't open file %s\n", {fullpath})
			abort(1)
		end if  

		
		sequence db = classFunc()

		--toPrintFunc(res)
		TokenizingPrintFunc(res)
		--trace(1)
		ParsingPrintFunc(res)
		trace(1)
		trace(0)
		
		--close(fn_vm)
		close(fn_jack)
		close(fn_xml)
		close(fn_Txml)

	end if
	return 0		
	
end function


--------Code stars here:--------

answer = prompt_string ("enter full path name to use:\n")
exit_code = walk_dir(answer, routine_id("look_at"), TRUE)
if exit_code = -1 then
	printf(OUT, "Folder doesn't exists\n")
else
	printf(OUT, "Executed\n")
end if

any_key ("\n   Press any key to close this Window... ")





