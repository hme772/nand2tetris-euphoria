//pop to D
	@SP
	M=M-1
	A=M
	D=M
	
	
//pop to A
	@SP
	M=M-1
	A=M
	A=M
	

//add
	pop to D
	@SP
	A=M-1
	M=M+D
	
	
//sub
	pop to D
	@SP
	A=M-1
	M=M-D
	
	
//neg
	pop to D
	@SP
	A=M
	M=-D
	@SP
	M=M+1
	
//eq
	pop to D
	pop to A
	D=D-A
	@IF_TRUE_i
	D; JEQ
//	IF_FALSE	// 	push constant 0
	@SP
	A=M
	M=0
	@end_i
	0; JMP
	(IF_TRUE_i)	// 	push constant -1
	@SP
	A=M
	M=-1
	(end_i)
	@SP			//move the stack pointer
	M=M+1

	
//gt
	pop to D
	pop to A
	D=A-D
	@ifTrue_i
	D; JGT
//	ifFalse	// 	push constant 0
	@SP
	A=M
	M=0
	@end_i
	0; JMP
	(ifTrue_i)	// 	push constant -1
	@SP
	A=M
	M=-1
	(end_i)
	@SP			//move the stack pointer
	M=M+1

	
//lt
	pop to D
	pop to A
	D=A-D
	@IF_TRUE_i
	D; JLT
//	IF_FALSE	// 	push constant -1
		@SP
		A=M
		M=0
		@end_i
		0; JMP
	(IF_TRUE_i)	// 	push constant 0
		@SP
		A=M
		M=-1
	(end_i)
		@SP			//move the stack pointer
		M=M+1
		


//and		אפשרות אחרת
	pop to D
	@SP
	A=M-1
	M=D&M

	
//or
	pop to D
	@SP
	A=M-1
	M=D|M
	

//not
	@SP
	A=M-1
	M=!M


--------------------------------- POP ------------------------------------------
	
	
// RAM[13] = RAM[segment] + i		// SUB-FUNC: save segment location + i in @13
	@i
	D=A
	@segment
	D=M+D
	@13
	M=D

	
// pop segment i
	RAM[13] = RAM[segment] + i
	pop to D
	@13
	A=M
	M=D
--------------------------------- POP 2 ------------------------------------------
	@i
	D=A
	@segment
	D=A+D
	@13
	M=D
	
// pop segment i
	RAM[13] = RAM[segment] + i
	pop to D
	@13
	A=M
	M=D
--------- POP to static ------

	pop to D
	@xxx.i
	M=D

--------------------------------- PUSH ------------------------------------------


// D = RAM[ RAM[segment] + i ]		// SUB-FUNC: save in D the value that store in segment location + i 
	@i
	D=A
	@segment
	A=M+D
	D=M


// push segment i
	D = RAM[ RAM[segment] + i ]
	@SP
	A=M
	M=D			//push the value to the stack
	
	@SP
	M=M+1		// SP++

------------------- PUSH 2 -----------------------	
	
			// SUB-FUNC: save in D the value that store in segment location + i 
	@i
	D=A
	@segment
	A=A+D
	D=M
				// push segment i
	@SP
	A=M
	M=D			//push the value to the stack
	
	@SP
	M=M+1		// SP++
	
--------- Push static ------
	@xxx.i
	D=M
	@SP
	A=M
	M=D

	@SP
	M=M+1		// SP++
	
--------- Push constant ------
	@offset
	D=A
	@SP
	A=M
	M=D
	@SP
	M=M+1		// SP++
	

------ PUSH D -----------
	@SP
	A=M
	M=D
	
	@SP			//SP++
	M=M+1
	
--- call f n -------
	@return address
	//push D()
	
	@LCL
	D=M
	//push D()
	
	@ARG
	D=M
	//push D()
	
	@THIS
	D=M
	//push D()
	
	@THAT
	D=M
	//push D()
	
	//ARG = SP-n-5
	@SP
	D=M
	@n
	D=D-A
	@5
	D=D-A
	@ARG
	M=D
	
	//LCL = SP
	@SP
	D=M
	@LCL
	M=D
	
	// goto f
	@f
	0; JMP
	
	(return address)
	
----------- return -----------------
	
//FRAME = LCL
	@LCL
	D=M
	@13
	M=D

//RET = *(FRAME-5)
	@5
	D=D-A
	A=D
	D=M		// the return address
	@14
	M=D
	
// *ARG = POP()
	pop to D
	@ARG
	A=M
	M=D		// the return value
	
// SP = ARG+1
	D=A
	@SP
	M=D+1

// THAT = *(FRAME-1)
	@13
	A=M-1
	D=M		//*(frame-1)
	@THAT
	M=D
	
// THIS = *(FRAME-2)
	@13
	A=M-1
	A=A-1
	D=M		//*(frame-2)
	@THIS
	M=D

// ARG = *(FRAME-3)
	@13
	A=M-1
	A=A-1
	A=A-1
	D=M		//*(frame-3)
	@ARG
	M=D

// LCL = *(FRAME-4)
	@13
	A=M-1
	A=A-1
	A=A-1
	A=A-1
	D=M		//*(frame-4)
	@LCL
	M=D

// goto RET
	@14
	A=M
	0; JMP

	
	
	
	
	
	






