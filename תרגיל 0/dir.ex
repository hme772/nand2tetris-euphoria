#!/home/euphoria-4.0b2/bin/eui
--include std /filesys .e as
include file.e
include get.e
include std/regex.e as re 
include std/sequence.e
include std/convert.e

sequence stringVM
stringVM = {"vm"}
object stam1
integer currFile
integer count
count=1
function passOverFiles (sequence path, sequence item)
	sequence f=path&"\\"&item[D_NAME]
	currFile=open(f,"a")
	sequence vm= split_any(f,'.')
	stam1=vm[2]
	if equal(stam1,stringVM[1]) then
		printf(currFile,to_string(count))
		count+=1
	end if

	return 0
end function 
object exit_code
 exit_code = walk_dir("C:\\Users\\yael\\euphoria", routine_id("passOverFiles"),1)
